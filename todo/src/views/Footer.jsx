import React, { useState, useRef } from "react";
import styled from "styled-components";

// import styled from "styled-components";

const Footer = ({list,setter}) => {
    const buttonsWrapperElement = useRef(null);
    const buttonStyle = (target) => {
     console.log(buttonsWrapperElement);
     for(let i = 0; i < buttonsWrapperElement.current.children.length; i++){
         let child = buttonsWrapperElement.current.children[i];
         if(child.innerText === target.innerText){
             child.style.borderColor = "pink";
         }else{
            child.style.borderColor = "";
         }
     }
    };
    const deleteComp = () =>{
            const dupList = list.filter((item) => !item.marked);
            setter(dupList);
        }

    const displayActive=(e)=>{
        buttonStyle(e.target)
        let newList=list.map((item)=>{
            if(item.marked){
                return {...item,display:false};
            }
            else{
                return {...item,display:true};
            }
        });
        setter(newList);
        console.log(list);
    }
    const displayNonActive=(e)=>{
        buttonStyle(e.target)
        let newList=list.map((item)=>{
            if(item.marked){
                return {...item,display:true};
            }
            else{
                return {...item,display:false};
            }
        });
        setter(newList);
        console.log(list);
    }
    const displayAll=(e)=>{
        buttonStyle(e.target)
        let newList=[];
        for(let item of list) {
                newList.push({...item,display:true});
        }
        setter(newList);
        console.log(list);
    }

    return (list.length>0) ?  <Box>
        <p>{list.filter((x)=>!x.marked).length} items left</p>
        <div ref={buttonsWrapperElement} className="buttonsWrapper">
            <button onClick={displayAll}>All</button>
            <button onClick={displayActive}>Active</button>
            <button onClick={displayNonActive}>Completed</button>
        </div>
        {
        list.filter((x)=>x.marked).length>0 ? 
            <button className="deleteCompBTN" onClick={deleteComp}>Delete Completed</button>
            :
            null
}
    </Box> 
    :
    null
}

const Box=styled.div`
    display:flex;
    background-color:white;
    display:flex;
    justify-content:space-between;
    font-size:10px;
    padding-top:1rem;
    .buttonsWrapper{
    }
    .buttonsWrapper button{
        background-color:white;
        border:1px solid white;
        font-size:10px;
        padding:0 1rem ;
        cursor: pointer;
        outline: none;
        border-color:transparent
    }
    .buttonsWrapper button:hover{
        border-color:cyan;
    }
    .buttonsWrapper button:target{
        border-color:deeppink;
    }
    .deleteCompBTN{
        background-color:white;
        border:1px solid white; 
        font-size:10px;

    }
    .deleteCompBTN:hover{
        border-bottom: 1px solid black;
        cursor: pointer;
    }
`;

export default Footer;