import React, { useState } from "react";
import Header from "./Header"
import Form from "./Form"
import List from "./List"
import Footer from "./Footer"
import styled from "styled-components";

const Box = styled.div`
    margin:3rem;
    text-align: center;
    font-size:16px;
`;

const Wrapper = styled.div`
margin:1rem;
padding:1rem;
background-color:white;
box-shadow:0 1px 4px rgba(0, 0, 0, 0.3), 0 0 40px rgba(0, 0, 0, 0.1) ;
    .effect8:before, .effect8:after
{
  content:"";
    position:absolute;
    z-index:-1;
    box-shadow:0 0 20px rgba(0,0,0,0.8);
    top:10px;
    bottom:10px;
    left:0;
    right:0;
    border-radius:100px / 10px;
}
.effect8:after
{
  right:10px;
    left:auto;
            transform:skew(8deg) rotate(3deg);
}
`;


const TodoApp = () => {
    const [list, set_list] = useState([]);
    return <>
    <Box>
        <Header/>
        <Wrapper>
            <Form list={list} setter={set_list}/>
            <List list={list} setter={set_list}/>
            <Footer list={list} setter={set_list}/>
        </Wrapper> 
    </Box>
    
    </>
};

export default TodoApp;