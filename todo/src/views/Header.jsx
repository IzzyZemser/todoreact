import React, { useState } from "react";
import styled from "styled-components";

const H1 = styled.h1`
font-family: -apple-system,BlinkMacSystemFont,"Roboto","Oxygen", "Ubuntu","Cantarell","Fira Sans","Droid Sans","Helvetica Neue", sans-serif;
font-weight: 500;
font-size: 2.5rem;`

const Header = () => {
    return <H1>Todo App</H1>
}

export default Header
