import React, { useState, useRef } from "react";
import styled from "styled-components";

let id = 0

const Form = ({list, setter}) => {
    const inputRef = useRef(null);
    const addToList = (e) => {
        e.preventDefault()
        setter([...list, {value:inputRef.current.value,marked:false, display:true,id:id++}])
        inputRef.current.value = "";
    }
    return <FormTag>
            <input placeHolder="Your Next Task" ref={inputRef}/>
            <button onClick={addToList}>add</button>
        </FormTag>
}


const FormTag=styled.form`
    display:flex;
    input{
        min-height:3rem;
        width:50rem;
        padding: 0.5rem;
        font-size: 2.4rem;
        font-weight: 300;

    }
    button{
        min-height:3rem;
        min-width: 7rem;
        padding: 0.5rem;
        font-weight: 300;
        cursor: pointer;
    }
`;

export default Form